def write_lammps(structure, filename, cell=None, Nelements=0):
    if cell == None:
        raise Exception('Need to specify cell keyword, anythng else not yet implemented')
    structure.center(about=0.0)
    syms = structure.get_chemical_symbols()
    elements = list(set(syms))
    if Nelements == 0:
        Nelements = len(elements)
    assert(Nelements < 3)
    poss = structure.get_positions()
    
    with open(filename, 'w') as f:
        print >>f, '# Generated from ASE structure'
        print >>f
        print >>f, '%d atoms' % (len(structure))
        print >>f
        print >>f, '%d atom types' % Nelements
        print >>f
        print >>f, '%f %f xlo xhi' % (cell[0], cell[1])
        print >>f, '%f %f ylo yhi' % (cell[2], cell[3])
        print >>f, '%f %f zlo zhi' % (cell[4], cell[5])
        print >>f
        print >>f, 'Atoms'
        print >>f
        for i in range(len(structure)):
            if syms[i] == elements[0]:
                atomtype = 1
            elif syms[i] == elements[1]:
                atomtype = 2
            else:
                raise Exception()
            print >>f, i+1,
            print >>f, atomtype,
            for p in poss[i]:
                print >>f, p,
            print >>f


def write_traj(structure, filename, cell=None):
    if cell == None:
        raise Exception('Need to specify cell keyword, anythng else not yet implemented')
    structure.center(about=0.0)
    syms = structure.get_chemical_symbols()
    elements = list(set(syms))
    poss = structure.get_positions()

    with open(filename, 'a') as f:
        print >>f, 'ITEM: TIMESTEP'
        print >>f, '0'
        print >>f, 'ITEM: NUMBER OF ATOMS'
        print >>f, '%d' % len(syms)
        print >>f, 'ITEM: BOX BOUNDS ff ff ff'
        print >>f, '%f %f' % (cell[0], cell[1])
        print >>f, '%f %f' % (cell[2], cell[3])
        print >>f, '%f %f' % (cell[4], cell[5])
        print >>f, 'ITEM: ATOMS id type x y z'
        for i in range(len(structure)):
            el = syms[i]

            elid = -10
            for j, testelem in enumerate(elements):
                if el == testelem:
                    elid = j
                    break
            if elid == -10:
                raise Exception()
            print >>f, '%d %d %f %f %f' % (i, elid, poss[i][0], poss[i][1], poss[i][2])
