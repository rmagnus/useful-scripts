import subprocess
from subprocess import call
import os
import numpy as np


def atat_print_structure_from_atoms(atoms, filename='str.in'):
    with open(filename, 'w') as f:
        for i in range(3):
            for j in range(3):
                if i == j:
                    f.write('{:12.6f}'.format(1.0))
                else:
                    f.write('{:12.6f}'.format(0.0))
            f.write('\n')

        cell = atoms.get_cell()
        for row in cell:
            for r in row:
                f.write('{:12.6f}'.format(r))
            f.write('\n')

        for poss, element in zip(atoms.get_positions(),
                                 atoms.get_chemical_symbols()):
            for pos in poss:
                f.write('{:12.6f}'.format(pos))
            f.write('  {}\n'.format(element))


def atat_print_lattice_from_atoms(atoms, subelements, filename='lat.in'):
    with open(filename, 'w') as f:
        for i in range(3):
            for j in range(3):
                if i == j:
                    f.write('{:12.6f}'.format(1.0))
                else:
                    f.write('{:12.6f}'.format(0.0))
            f.write('\n')

        cell = atoms.get_cell()
        for row in cell:
            for r in row:
                f.write('{:12.6f}'.format(r))
            f.write('\n')

        for poss in atoms.get_positions():
            for pos in poss:
                f.write('{:12.6f}'.format(pos))
            f.write('   ')
            for i, element in enumerate(subelements):
                if i > 0:
                    f.write(',')
                f.write('{}'.format(element))
            f.write('\n')


def atat_create_cluster_space(structure,
                              cutoffs,
                              subelements,
                              lattice_file='lat.in',
                              structure_file='str.in',
                              remove_needless_files=True):
    atat_print_lattice_from_atoms(
        structure, subelements, filename=lattice_file)
    atat_print_structure_from_atoms(structure)
    command = ['corrdump']
    for i, cutoff in enumerate(cutoffs):
        command.append('-{}={}'.format(i + 2, cutoff))
    command.append('-l={}'.format(lattice_file))
    command.append('-s={}'.format(structure_file))
    process = subprocess.run(command, stdout=subprocess.PIPE)

    if remove_needless_files:
        os.remove(structure_file)
        os.remove('corrdump.log')
        os.remove('sym.out')


def atat_get_cluster_vector(structure,
                            lattice='lat.in',
                            structure_file='str.in',
                            remove_needless_files=True):
    atat_print_structure_from_atoms(structure, filename='str.in')
    command = ['corrdump']
    command.append('-c')  # Use clusters.out
    command.append('-s={}'.format(structure_file))
    command.append('-sig=15') # Print 10 significant digits instead of 5
    process = subprocess.run(command, stdout=subprocess.PIPE)
    cv = [float(i) for i in process.stdout.decode("utf-8").split()]

    if remove_needless_files:
        os.remove(structure_file)

    return np.array(cv)


def remove_atat_files():
    files = ['lat.in',
             'str.in',
             'clusters.out',
             'sym.out',
             'corrdump.log']
    for filename in files:
        if os.path.isfile(filename):
            os.remove(filename)
