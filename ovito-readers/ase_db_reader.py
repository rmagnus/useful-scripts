from ovito.data import *
from ovito.io.ase import *
from typing import Callable
import traits.api

try:
    from ase.db import connect
except ImportError:
    raise ImportError('This file reader requires the ASE Python package. '
        'Please install it first by running "pip install ase" or, if you are '
        'using the embedded interpreter of OVITO Pro, "ovitos -m pip install ase".')

class FileReader(traits.api.HasStrictTraits):

    # Optional filter expression that selects only certain structures from the ASE database.
    query_string = traits.api.Str(label='ASE query string')

    @staticmethod
    def detect(filename : str):
        # This will raise an exception if the file is not a valid ASE database file.
        try: connect(filename).count()
        except: return False
        return True

    def scan(self, filename : str, register_frame : Callable[..., None]):
        # Open the ASE database file selected by the user and
        # step through the rows of the database treating each as a separate trajectory frame.
        db = connect(filename) 
        for row in db.select(self.query_string, verbosity=2):
            register_frame(parser_data=row.id, label=row.formula)

    def parse(self, data : DataCollection, **kwargs):
        db = connect(kwargs['filename'])
        # Load the db row corresponding to the current frame number. 
        # Its db ID was stored in the 'parser_data' field by the scan() method. 
        ase_atoms = db.get_atoms(kwargs['parser_data'], add_additional_information=True)
        # Convert the ASE Atoms object to the canonical OVITO representation.
        ase_to_ovito(ase_atoms, data)