from ovito.data import *
from ovito.io.ase import *
from typing import Callable

try:
    from ase.io import read
except ImportError:
    raise ImportError('This file reader requires the ASE Python package. '
        'Please install it first by running "pip install ase" or, if you are '
        'using the embedded interpreter of OVITO Pro, "ovitos -m pip install ase".')

class FileReader:

    @staticmethod
    def detect(filename : str):
        try:
            read(filename)
            return True
        except: 
            return False

    @staticmethod
    def scan(filename : str, register_frame : Callable[..., None]):
        for i, traj in enumerate(read(filename, index=':')):
            register_frame(parser_data=i)

    @staticmethod
    def parse(data : DataCollection, **kwargs):
        ase_atoms = read(kwargs['filename'], index=kwargs['parser_data'])
        ase_to_ovito(ase_atoms, data)