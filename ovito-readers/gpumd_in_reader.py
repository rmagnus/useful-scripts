from ovito.data import *
from ovito.io.ase import *
from ovito.io import FileReaderInterface
from typing import Callable
from calorine.io import read_xyz

class FileReader(FileReaderInterface):

    @staticmethod
    def detect(filename : str):
        try:
            read_xyz(filename)
            return True
        except: 
            return False

    #@staticmethod
    #def scan(filename : str, register_frame : Callable[..., None]):
    #    for i, traj in enumerate(read(filename, index=':')):
    #        register_frame(parser_data=i)

    @staticmethod
    def parse(data : DataCollection, **kwargs):
        ase_atoms, _ = read_xyz(kwargs['filename'])
        ase_to_ovito(ase_atoms, data)