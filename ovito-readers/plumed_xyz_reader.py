from ovito.data import *
from ovito.io.ase import *
from typing import Callable
from ase import Atoms

class FileReader:

    @staticmethod
    def detect(filename : str):
        with open(filename, 'r') as f:
            count = 0
            for line in f:
                if count == 0:
                    row = line.split()
                    if len(row) != 1:
                        return False
                    try:
                        int(row[0])
                    except:
                        return False
                elif count == 1:
                    row = line.split()
                    if len(row) != 3:
                        return False
                    try:
                        int([float(i) for i in row])
                    except:
                        return False
                elif count == 2:
                    row = line.split()
                    if len(row) != 4:
                        return False
                    try:
                        int([float(i) for i in row[1:]])
                    except:
                        return False
                else:
                    return True
                count += 1

    @staticmethod
    def scan(filename : str, register_frame : Callable[..., None]):
        count = 0
        with open(filename, 'r') as f:
            for line in f:
                row = line.split()
                if len(row) == 1:
                    register_frame(parser_data=count)
                    count += 1

    @staticmethod
    def parse(data : DataCollection, **kwargs):
        symbols = []
        positions = []
        with open(kwargs['filename'], 'r') as f:
            count = 0
            for line in f:
                row = line.split()

                if collect_atoms:
                    if len(row) == 1:
                        break
                    elif len(row) == 3:
                        cell = [float(i) for i in row]
                    else:
                        symbols.append(row[0])
                        positions.append([float(i) for i in row[1:]])
                else:
                    if len(row) == 1:
                        if count == kwargs['parser_data']:
                            collect_atoms = True
                        else:
                            count += 1
        ase_atoms = Atoms(symbols=symbols, positions=positions, cell=cell)
        ase_to_ovito(ase_atoms, data)